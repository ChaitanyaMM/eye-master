To access this project simply run KafkaSampleApplication.java file
This file has both producer and consumer acces it will print message what producer sent!.


To run kafka follow below commands for linux cluster
========================================================
bin/zookeeper-server-start.sh config/zookeeper.properties 

bin/kafka-server-start.sh config/server.properties   

bin/kafka-topics.sh �create �zookeeper localhost:2181 �replication-factor 1 �partitions 1 �topic eyedentify-topic

bin/kafka-console-producer.sh --broker-list  localhost:9092 --topic eyedentify-topic

bin/kafka-topics.sh -list --zookeeper localhost:2181

bin/kafka-console-consumer.sh --bootstrap-server localhost:9092 --from-beginning --topic eyedentify-topic

